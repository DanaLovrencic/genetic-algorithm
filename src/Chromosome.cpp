#include "Chromosome.hpp"
#include "Random.hpp"

#include <cmath>
#include <iomanip>

#include <iostream>

Chromosome::Chromosome(const std::vector<Bounds>& bounds) : bounds(bounds) {}

void Chromosome::set_fitness(double fitness)
{
    this->fitness = fitness;
}

double Chromosome::get_fitness() const
{
    if (std::isnan(fitness)) { throw std::logic_error("Fitness not defined."); }
    return fitness;
}

std::size_t Chromosome::size() const
{
    return phenotype.size();
}

std::vector<Bounds> Chromosome::get_bounds() const
{
    return bounds;
}

double Chromosome::operator[](unsigned index) const
{
    return phenotype[index];
}

double& Chromosome::operator[](unsigned index)
{
    return phenotype[index];
}

std::ostream& operator<<(std::ostream& os, const Chromosome& c)
{
    c.to_stream(os);
    return os;
}

std::vector<double>::const_iterator Chromosome::begin() const
{
    return phenotype.begin();
}

std::vector<double>::const_iterator Chromosome::end() const
{
    return phenotype.end();
}

BinaryChromosome::BinaryChromosome(const std::vector<Bounds>& bounds,
                                   unsigned precision)
    : Chromosome(bounds), precision(precision), genotype(bounds.size())
{
    phenotype.resize(bounds.size());

    // Calculate length of each binary represented variable.
    for (std::size_t i = 0; i < bounds.size(); i++) {
        unsigned n = std::ceil(
            std::log10(std::floor(1 + (bounds[i].upper - bounds[i].lower) *
                                          std::pow(10, precision))) /
            std::log10(2));
        genotype[i].resize(n);
        chromosome_size += n;
    }

    randomize();
}

double BinaryChromosome::get_precision() const
{
    return precision;
}

std::size_t BinaryChromosome::get_chromosome_size() const
{
    return chromosome_size;
}

std::vector<bool>& BinaryChromosome::at(std::size_t index)
{
    return genotype[index];
}

std::vector<bool>::reference BinaryChromosome::at(std::size_t index1,
                                                  std::size_t index2)
{
    return genotype[index1][index2];
}

std::vector<bool>& BinaryChromosome::operator[](unsigned index)
{
    return genotype[index];
}

void BinaryChromosome::flip_random_bits()
{
    for (auto& var : genotype) {
        double prob = 1.0 / var.size();
        std::bernoulli_distribution dist(prob);
        for (auto bit : var) {
            if (dist(Random::generator()) == 1) { bit.flip(); }
        }
    }
}

std::unique_ptr<Chromosome> BinaryChromosome::clone() const
{
    return std::make_unique<BinaryChromosome>(*this);
}

void BinaryChromosome::randomize()
{
    static std::bernoulli_distribution dist; // Equal probability of 0 and 1.
    for (auto& el : genotype) {
        for (auto b : el) { b = dist(Random::generator()); }
    }
    set_phenotype_from_genotype();
}

std::vector<std::vector<bool>>::const_iterator BinaryChromosome::begin() const
{
    return genotype.begin();
}

std::vector<std::vector<bool>>::const_iterator BinaryChromosome::end() const
{
    return genotype.end();
}

void BinaryChromosome::to_stream(std::ostream& os) const
{
    std::ios_base::fmtflags flags(os.flags());

    for (std::vector<bool> el : genotype) {
        for (std::vector<bool>::const_reference b : el) { os << b; }
        os << " ";
    }
    os << "| " << std::fixed << std::setprecision(4);
    for (const auto& el : phenotype) { os << std::setw(8) << el << ' '; }
    os.flags(flags);
    os << " |  FITNESS: " << fitness;
    // os.flags(flags);
}

void BinaryChromosome::set_phenotype_from_genotype()
{
    for (std::size_t i = 0; i < phenotype.size(); i++) {
        int genotype_as_int = binary_to_int(genotype[i]);
        phenotype[i]        = bounds[i].lower +
                       genotype_as_int * (bounds[i].upper - bounds[i].lower) /
                           (std::pow(2, genotype[i].size()) - 1);
    }
}

int BinaryChromosome::binary_to_int(const std::vector<bool>& binary)
{
    int int_value = 0;
    for (std::size_t i = 0; i < binary.size(); i++) {
        int_value += binary[i] * std::pow(2, binary.size() - 1 - i);
    }

    return int_value;
}

FloatChromosome::FloatChromosome(const std::vector<Bounds>& bounds)
    : Chromosome(bounds)
{
    phenotype.resize(bounds.size());
    randomize();
}

std::unique_ptr<Chromosome> FloatChromosome::clone() const
{
    return std::make_unique<FloatChromosome>(*this);
}

void FloatChromosome::randomize()
{
    for (std::size_t i = 0; i < phenotype.size(); i++) {
        std::uniform_real_distribution<double> dist(bounds[i].lower,
                                                    bounds[i].upper);
        phenotype[i] = dist(Random::generator());
    }
}

void FloatChromosome::to_stream(std::ostream& os) const
{
    for (const auto& el : phenotype) {
        os << std::setw(12) << std::left << el << " ";
    }
    os << " =>  " << fitness;
}

std::ostream& operator<<(std::ostream& os, const std::vector<Chromosome*>& v)
{
    for (const auto& el : v) { os << *el << '\n'; }
    return os;
}
