#include "Bounds.hpp"

Bounds::Bounds(double lower, double upper) : lower(lower), upper(upper) {}

std::ostream& operator<<(std::ostream& os, const Bounds& bounds)
{
    return os << '(' << bounds.lower << ", " << bounds.upper << ')';
}
