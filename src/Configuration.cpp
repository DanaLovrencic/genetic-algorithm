#include "Configuration.hpp"

namespace {

Bounds create_bounds(const std::string& bounds_str);

std::filesystem::path create_filepath(const std::string& filepath);

std::unique_ptr<FitnessFunction>
create_fitness_function(const std::string& name,
                        std::optional<std::filesystem::path> filepath);

std::unique_ptr<Chromosome>
create_initial_chromosome(unsigned dimensionality,
                          const std::string& chromosome_type,
                          Bounds b,
                          std::optional<unsigned> precision);

std::unique_ptr<Crossover> create_crossover(const std::string& name);
std::unique_ptr<Mutation> create_mutation(const std::string& name,
                                          double mut_prob);
}

Configuration::Configuration(const cxxopts::ParseResult& pr)
    : n_of_iters(pr["n-iter"].as<unsigned>()),
      pop_size(pr["pop-size"].as<std::size_t>()),
      mut_prob(pr["mut-prob"].as<double>()),
      b(create_bounds(pr["bounds"].as<std::string>()))
{
    if (pr.count("precision") > 0) precision = pr["precision"].as<unsigned>();

    if (pr.count("filepath") > 0) {
        filepath = create_filepath(pr["filepath"].as<std::string>());
    }

    fitness_function =
        create_fitness_function(pr["fitness-func"].as<std::string>(), filepath);

    initial_chromosome =
        create_initial_chromosome(pr["dimensionality"].as<unsigned>(),
                                  pr["chromosome-type"].as<std::string>(),
                                  b,
                                  precision);

    crossover = create_crossover(pr["crossover"].as<std::string>());

    mutation = create_mutation(pr["mutation"].as<std::string>(),
                               pr["mut-prob"].as<double>());

    // TODO: Validate configuration.
}

namespace {
Bounds create_bounds(const std::string& bounds_str)
{
    double lower, upper;

    const std::string delimiter = ",";
    std::size_t delimiter_pos   = bounds_str.find(delimiter);
    if (delimiter_pos == std::string::npos || delimiter_pos == 0 ||
        delimiter_pos == bounds_str.length()) {
        std::stringstream ss;
        ss << "Invalid bounds string format: " << bounds_str
           << "\nExpected format: <lower>,<upper>";
        throw std::runtime_error(ss.str());
    }
    {
        std::string lower_str = bounds_str.substr(0, delimiter_pos);
        std::stringstream ss;
        ss << lower_str;
        ss >> lower;
    }
    {
        std::string upper_str = bounds_str.substr(
            delimiter_pos + delimiter.length(), bounds_str.length());
        std::stringstream ss;
        ss << upper_str;
        ss >> upper;
    }
    return Bounds(lower, upper);
}

std::unique_ptr<FitnessFunction>
create_fitness_function(const std::string& name,
                        std::optional<std::filesystem::path> filepath)
{
    std::string fnl = name; // Function name as lower characters.
    std::transform(fnl.begin(), fnl.end(), fnl.begin(), [](unsigned char c) {
        return std::tolower(c);
    });
    if (fnl == "f1") { return std::make_unique<F1>(); }
    if (fnl == "f3") { return std::make_unique<F3>(); }
    if (fnl == "f6") { return std::make_unique<F6>(); }
    if (fnl == "f7") { return std::make_unique<F7>(); }
    if (fnl == "f") {
        if (!filepath.has_value()) {
            throw std::runtime_error("Filepath is not initialized!");
        }
        return std::make_unique<F>(true, *filepath);
    }

    std::stringstream ss;
    ss << "Unknown fitness function name: " << name
       << "\nAvailable function names: f, f1, f3, f6, f7";
    throw std::runtime_error(ss.str());
}

std::unique_ptr<Chromosome>
create_initial_chromosome(unsigned dimensionality,
                          const std::string& chromosome_type,
                          Bounds b,
                          std::optional<unsigned> precision)
{
    std::vector<Bounds> bounds(dimensionality, b);

    std::string ctl = chromosome_type; // Chromosome type as lower characters.
    std::transform(ctl.begin(), ctl.end(), ctl.begin(), [](unsigned char c) {
        return std::tolower(c);
    });

    if (ctl == "binary") {
        if (!precision.has_value()) {
            throw std::runtime_error(
                "Binary chromosome requires precision to be set.");
        }
        return std::make_unique<BinaryChromosome>(bounds, precision.value());
    }
    if (ctl == "float") return std::make_unique<FloatChromosome>(bounds);

    std::stringstream ss;
    ss << "Unknown chromosome type: " << chromosome_type
       << "\nAvailable chromosome types: binary, float";
    throw std::runtime_error(ss.str());
}

std::unique_ptr<Crossover> create_crossover(const std::string& name)
{
    std::string ntl = name; // Crossover type name as lower characters.
    std::transform(ntl.begin(), ntl.end(), ntl.begin(), [](unsigned char c) {
        return std::tolower(c);
    });
    if (ntl == "arithmetic") return std::make_unique<ArithmeticCrossover>();
    if (ntl == "average") return std::make_unique<ArithmeticCrossover>();
    if (ntl == "single-point") return std::make_unique<SinglePointCrossover>();
    if (ntl == "uniform") return std::make_unique<UniformCrossover>();

    std::stringstream ss;
    ss << "Unknown crossover type name: " << name
       << "\nAvailable crossovers: arithmetic, average, single-point, uniform";
    throw std::runtime_error(ss.str());
}

std::unique_ptr<Mutation> create_mutation(const std::string& name,
                                          double mut_prob)
{
    std::string ntl = name; // Mutation type name as lower characters.
    std::transform(ntl.begin(), ntl.end(), ntl.begin(), [](unsigned char c) {
        return std::tolower(c);
    });
    if (ntl == "simple")
        return std::make_unique<SimpleBinaryMutation>(mut_prob);
    if (ntl == "gauss") return std::make_unique<GaussianMutation>(mut_prob);

    std::stringstream ss;
    ss << "Unknown mutation type name: " << name
       << "\nAvailable mutations: simple, gauss";
    throw std::runtime_error(ss.str());
}

std::filesystem::path create_filepath(const std::string& filepath)
{
    return filepath;
}
}
