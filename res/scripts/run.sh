#!/bin/bash
EXE=./LAB4
FILE=res/results/out.txt # out1
rm -f ${FILE}

$EXE --chromosome-type float \
     --bounds -4,4 \
     --dimensionality 5 \
     --fitness-func f \
     --n-iter 4000 \
     --pop-size 50 \
     --mutation gauss \
     --mut-prob 0.03 \
     --crossover arithmetic \
     --filepath res/datasets/dataset1.txt >> ${FILE}
