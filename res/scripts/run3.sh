#!/bin/sh
EXE=./LAB4
FILE=res/results/out3.txt
rm -f ${FILE}
DIMS=(3 6)

for d in "${DIMS[@]}"
do
   printf '%d: ' $d >> ${FILE}
   for i in {1..30}
   do
    $EXE --chromosome-type float \
         --bounds -50,150 \
         --dimensionality $d \
         --fitness-func F6 \
         --n-iter 400000 \
         --pop-size 50 \
         --mutation gauss \
         --mut-prob 0.5 \
         --crossover arithmetic | \
              awk '{print $NF}' | \
              sed -z 's/\n/ /g' >> ${FILE}
   done
   echo >> ${FILE}
done

for d in "${DIMS[@]}"
do
   printf '%d: ' $d >> ${FILE}
   for i in {1..30}
   do
    $EXE --chromosome-type float \
         --bounds -50,150 \
         --dimensionality $d \
         --fitness-func F7 \
         --n-iter 400000 \
         --pop-size 50 \
         --mutation gauss \
         --mut-prob 0.5 \
         --crossover arithmetic | \
              awk '{print $NF}' | \
              sed -z 's/\n/ /g' >> ${FILE}
   done
   echo >> ${FILE}
done

for d in "${DIMS[@]}"
do
   printf '%d: ' $d >> ${FILE}
   for i in {1..30}
   do
    $EXE --chromosome-type binary \
         --bounds -50,150 \
         --dimensionality $d \
         --fitness-func F6 \
         --n-iter 400000 \
         --pop-size 50 \
         --mutation simple \
         --mut-prob 0.3 \
         --crossover single-point \
         --precision 4 \
              | awk '{print $NF}' \
              | sed -z 's/\n/ /g' >> ${FILE}
   done
   echo >> ${FILE}
done

for d in "${DIMS[@]}"
do
   printf '%d: ' $d >> ${FILE}
   for i in {1..30}
   do
    $EXE --chromosome-type binary \
         --bounds -50,150 \
         --dimensionality $d \
         --fitness-func F7 \
         --n-iter 400000 \
         --pop-size 50 \
         --mutation simple \
         --mut-prob 0.3 \
         --crossover single-point \
         --precision 4 \
              | awk '{print $NF}' \
              | sed -z 's/\n/ /g' >> ${FILE}
   done
   echo >> ${FILE}
done
