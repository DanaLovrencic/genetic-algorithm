#!/bin/sh
EXE=./LAB4
FILE=res/results/out2.txt
rm -f ${FILE}
DIMS=(1 3 6 10)
for d in "${DIMS[@]}"
do
    $EXE --chromosome-type float \
         --bounds -50,150 \
         --dimensionality $d \
         --fitness-func F6 \
         --n-iter 250000 \
         --pop-size 50 \
         --mutation gauss \
         --mut-prob 0.3 \
         --crossover arithmetic >> ${FILE}
done
echo >> ${FILE}

for d in "${DIMS[@]}"
do
    $EXE --chromosome-type float \
         --bounds -50,150 \
         --dimensionality $d \
         --fitness-func F7 \
         --n-iter 500000 \
         --pop-size 40 \
         --mutation gauss \
         --mut-prob 0.5 \
         --crossover arithmetic >> ${FILE}
done
echo >> ${FILE}
