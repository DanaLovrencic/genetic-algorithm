#!/bin/bash
EXE=./LAB4
FILE=res/results/out1.txt # out1
rm -f ${FILE}

$EXE --chromosome-type float \
     --bounds -50,150 \
     --dimensionality 2 \
     --fitness-func f1 \
     --n-iter 150000 \
     --pop-size 30 \
     --mutation gauss \
     --mut-prob 0.5 \
     --crossover arithmetic >> ${FILE}

$EXE --chromosome-type float \
     --bounds -50,150 \
     --dimensionality 5 \
     --fitness-func f3 \
     --n-iter 150000 \
     --pop-size 30 \
     --mutation gauss \
     --mut-prob 0.5 \
     --crossover arithmetic >> ${FILE}

$EXE --chromosome-type float \
     --bounds -50,150 \
     --dimensionality 2 \
     --fitness-func f6 \
     --n-iter 150000 \
     --pop-size 30 \
     --mutation gauss \
     --mut-prob 0.5 \
     --crossover arithmetic >> ${FILE}

$EXE --chromosome-type float \
     --bounds -50,150 \
     --dimensionality 2 \
     --fitness-func f7 \
     --n-iter 150000 \
     --pop-size 30 \
     --mutation gauss \
     --mut-prob 0.5 \
     --crossover arithmetic >> ${FILE}

$EXE --chromosome-type binary \
     --bounds -50,150 \
     --dimensionality 2 \
     --fitness-func f1 \
     --n-iter 850000 \
     --pop-size 30 \
     --mutation simple \
     --mut-prob 0.5 \
     --crossover single-point \
     --precision 4 >> ${FILE}

$EXE --chromosome-type binary \
     --bounds -50,150 \
     --dimensionality 5 \
     --fitness-func f3 \
     --n-iter 850000 \
     --pop-size 50 \
     --mutation simple \
     --mut-prob 0.5 \
     --crossover single-point \
     --precision 4 >> ${FILE}

$EXE --chromosome-type binary \
     --bounds -50,150 \
     --dimensionality 2 \
     --fitness-func f6 \
     --n-iter 850000 \
     --pop-size 50 \
     --mutation simple \
     --mut-prob 0.5 \
     --crossover uniform \
     --precision 4 >> ${FILE}

$EXE --chromosome-type binary \
     --bounds -50,150 \
     --dimensionality 2 \
     --fitness-func f7 \
     --n-iter 950000 \
     --pop-size 100 \
     --mutation simple \
     --mut-prob 0.5 \
     --crossover single-point \
     --precision 4 >> ${FILE}
