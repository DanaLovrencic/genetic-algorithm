set terminal x11 size 1500,800
set style fill solid 0.25 border -1
set style boxplot outliers pointtype 7
set style data boxplot

set tics font "Hack, 13"

set title 'Box Plot' font 'Hack,18';
plot for [i=1:16] 'res/results/out4transformed.txt' using (i):i notitle
