#ifndef BOUNDS_HPP
#define BOUNDS_HPP

#include <ostream>

struct Bounds {
    double lower;
    double upper;

    Bounds(double lower, double upper);
    friend std::ostream& operator<<(std::ostream& os, const Bounds& bounds);
};

#endif
