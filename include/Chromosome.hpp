#ifndef CHROMOSOME_HPP
#define CHROMOSOME_HPP

#include <limits>
#include <memory>
#include <vector>

#include "Function.hpp"
#include "Bounds.hpp"

class Chromosome {
  protected:
    std::vector<Bounds> bounds;
    std::vector<double> phenotype;
    double fitness = std::numeric_limits<double>::quiet_NaN();

  public:
    Chromosome(const std::vector<Bounds>& bounds);
    Chromosome() = default;

    void set_fitness(double fitness);
    double get_fitness() const;
    std::size_t size() const;
    std::vector<Bounds> get_bounds() const;

    virtual std::unique_ptr<Chromosome> clone() const = 0;
    virtual void randomize()                          = 0;

    std::vector<double>::const_iterator begin() const;
    std::vector<double>::const_iterator end() const;

    double operator[](unsigned index) const;
    double& operator[](unsigned index);

    friend std::ostream& operator<<(std::ostream& os, const Chromosome& c);

  private:
    virtual void to_stream(std::ostream& os) const = 0;
};

class BinaryChromosome : public Chromosome {
  private:
    unsigned precision;
    std::vector<std::vector<bool>> genotype;
    std::size_t chromosome_size = 0;

  public:
    BinaryChromosome(const std::vector<Bounds>& bounds, unsigned precision);

    double get_precision() const;
    std::size_t get_chromosome_size() const;
    std::vector<bool>& at(std::size_t index);
    std::vector<bool>::reference at(std::size_t index1, std::size_t index2);
    std::unique_ptr<Chromosome> clone() const override;
    void flip_random_bits();
    void randomize() override;
    void set_phenotype_from_genotype();

    std::vector<std::vector<bool>>::const_iterator begin() const;
    std::vector<std::vector<bool>>::const_iterator end() const;

    std::vector<bool>& operator[](unsigned index);

  private:
    int binary_to_int(const std::vector<bool>& binary);

    void to_stream(std::ostream& os) const override;
};

class FloatChromosome : public Chromosome {
  public:
    FloatChromosome(const std::vector<Bounds>& bounds);

    std::unique_ptr<Chromosome> clone() const override;
    void randomize() override;

  private:
    void to_stream(std::ostream& os) const override;
};

std::ostream& operator<<(std::ostream& os, const std::vector<Chromosome*>& v);

#endif
