#ifndef GENETIC_ALGORITHM
#define GENETIC_ALGORITHM

#include "Crossover.hpp"
#include "Mutation.hpp"
#include "Population.hpp"
#include "Function.hpp"

class GeneticAlgorithm {
  protected:
    Crossover& crossover;
    Mutation& mutation;
    const std::size_t pop_size;
    const unsigned n_iters = 10000;
    Chromosome* best;

  public:
    GeneticAlgorithm(Crossover& crossover,
                     Mutation& mutation,
                     std::size_t pop_size,
                     unsigned n_iters);

  private:
    virtual std::unique_ptr<Chromosome>
    optimize(FitnessFunction& fitness_function, Chromosome& c) = 0;
};

class Elimination3TournamentGA : public GeneticAlgorithm {
  public:
    Elimination3TournamentGA(Crossover& crossover,
                             Mutation& mutation,
                             std::size_t pop_size,
                             unsigned n_iters);

    std::unique_ptr<Chromosome> optimize(FitnessFunction& f,
                                         Chromosome& c) override;

  private:
    std::array<Chromosome*, 3>
    get_sorted_chromosomes(const Population& population);
};

class GenerationRouletteWheelGA : public GeneticAlgorithm {
  private:
    double D = 0; // Population goodness.

  public:
    GenerationRouletteWheelGA(Crossover& crossover,
                              Mutation& mutation,
                              std::size_t pop_size,
                              unsigned n_iters);

    std::unique_ptr<Chromosome> optimize(FitnessFunction& f,
                                         Chromosome& c) override;

  private:
    std::unique_ptr<Chromosome>
    get_random_chromosome(const Population& population) const;
    double calculate_pop_goodness(const Population& population);
};

#endif
