#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include "Bounds.hpp"
#include "Function.hpp"
#include "Crossover.hpp"
#include "Mutation.hpp"

#include <cxxopts/cxxopts.hpp>
#include <filesystem>

struct Configuration {
    unsigned n_of_iters;
    std::size_t pop_size;
    double mut_prob;
    Bounds b;
    std::optional<unsigned> precision;
    std::optional<std::filesystem::path> filepath;

    std::unique_ptr<FitnessFunction> fitness_function;
    std::unique_ptr<Chromosome> initial_chromosome;
    std::unique_ptr<Crossover> crossover;
    std::unique_ptr<Mutation> mutation;

    Configuration(const cxxopts::ParseResult& pr);
};

#endif
